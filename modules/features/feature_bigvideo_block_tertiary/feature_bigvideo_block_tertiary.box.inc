<?php
/**
 * @file
 * feature_bigvideo_block_tertiary.box.inc
 */

/**
 * Implements hook_default_box().
 */
function feature_bigvideo_block_tertiary_default_box() {
  $export = array();

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'bigvideo_box_one';
  $box->plugin_key = 'simple';
  $box->title = 'Languages fact';
  $box->description = 'Box One Tertiaruy';
  $box->options = array(
    'body' => array(
      'value' => 'With 800 languages, New York is the most linguistically diverse city in the world.',
      'format' => 'filtered_html',
    ),
    'additional_classes' => '',
  );
  $export['bigvideo_box_one'] = $box;

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'bigvideo_box_three';
  $box->plugin_key = 'simple';
  $box->title = 'Historical fact';
  $box->description = 'Box Three Tertiary';
  $box->options = array(
    'body' => array(
      'value' => 'The city of "New Amsterdam" was given to the Duke of York in 1664 as an 18th birthday present from his father. He renamed the city as "New York."',
      'format' => 'filtered_html',
    ),
    'additional_classes' => '',
  );
  $export['bigvideo_box_three'] = $box;

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'bigvideo_box_twio';
  $box->plugin_key = 'simple';
  $box->title = 'Trees fact';
  $box->description = 'Box two tertiaryu';
  $box->options = array(
    'body' => array(
      'value' => 'NYC homeowners can request that a tree be planted outside their homes for free.',
      'format' => 'filtered_html',
    ),
    'additional_classes' => '',
  );
  $export['bigvideo_box_twio'] = $box;

  return $export;
}
