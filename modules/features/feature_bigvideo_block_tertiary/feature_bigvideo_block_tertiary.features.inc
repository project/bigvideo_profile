<?php
/**
 * @file
 * feature_bigvideo_block_tertiary.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function feature_bigvideo_block_tertiary_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "boxes" && $api == "box") {
    return array("version" => "1");
  }
}
