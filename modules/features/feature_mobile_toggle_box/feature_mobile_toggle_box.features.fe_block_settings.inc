<?php
/**
 * @file
 * feature_mobile_toggle_box.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function feature_mobile_toggle_box_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['boxes-mobile_toggle'] = array(
    'cache' => -2,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'mobile_toggle',
    'module' => 'boxes',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bigvideo_theme' => array(
        'region' => 'header',
        'status' => 1,
        'theme' => 'bigvideo_theme',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  return $export;
}
