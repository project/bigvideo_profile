<?php
/**
 * @file
 * feature_mobile_toggle_box.box.inc
 */

/**
 * Implements hook_default_box().
 */
function feature_mobile_toggle_box_default_box() {
  $export = array();

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'mobile_toggle';
  $box->plugin_key = 'simple';
  $box->title = '';
  $box->description = 'Toggle mobile menu';
  $box->options = array(
    'body' => array(
      'value' => '<div>
<i class="fa fa-bars"></i>
</div>',
      'format' => 'full_html',
    ),
    'additional_classes' => 'mobile-toggle',
  );
  $export['mobile_toggle'] = $box;

  return $export;
}
