<?php
/**
 * @file
 * feature_mobile_toggle_box.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function feature_mobile_toggle_box_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "boxes" && $api == "box") {
    return array("version" => "1");
  }
}
