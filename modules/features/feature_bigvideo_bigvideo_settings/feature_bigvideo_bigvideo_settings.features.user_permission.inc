<?php
/**
 * @file
 * feature_bigvideo_bigvideo_settings.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function feature_bigvideo_bigvideo_settings_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer bigvideo'.
  $permissions['administer bigvideo'] = array(
    'name' => 'administer bigvideo',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'bigvideo',
  );

  return $permissions;
}
