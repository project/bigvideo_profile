<?php
/**
 * @file
 * feature_bigvideo_views_preview.features.inc
 */

/**
 * Implements hook_views_api().
 */
function feature_bigvideo_views_preview_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function feature_bigvideo_views_preview_image_default_styles() {
  $styles = array();

  // Exported image style: vehicle.
  $styles['vehicle'] = array(
    'label' => '450x500',
    'effects' => array(
      2 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 450,
          'height' => 500,
        ),
        'weight' => 2,
      ),
    ),
  );

  return $styles;
}
