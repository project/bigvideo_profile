<?php
/**
 * @file
 * feature_bigvideo_theme_settings.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function feature_bigvideo_theme_settings_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "boxes" && $api == "box") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_features_site_frontpage_features_default_frontpage().
 */
function feature_bigvideo_theme_settings_features_site_frontpage_features_default_frontpage() {
  return array(
    'enabled' => 'UUID:163d06d2-8968-4609-9c65-67150a0af6ff',
  );
}
