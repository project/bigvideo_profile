<?php
/**
 * @file
 * feature_bigvideo_views_categories.features.inc
 */

/**
 * Implements hook_views_api().
 */
function feature_bigvideo_views_categories_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
