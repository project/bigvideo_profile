<?php
/**
 * @file
 * feature_bigvideo_footer_menu.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function feature_bigvideo_footer_menu_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-footer-menu_boroughs:<front>
  $menu_links['menu-footer-menu_boroughs:<front>'] = array(
    'menu_name' => 'menu-footer-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Boroughs',
    'options' => array(
      'fragment' => 'block-views-slider-bigvideo-slider',
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-footer-menu_boroughs:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );
  // Exported menu link: menu-footer-menu_home:<front>
  $menu_links['menu-footer-menu_home:<front>'] = array(
    'menu_name' => 'menu-footer-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Home',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-footer-menu_home:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Exported menu link: menu-footer-menu_learn-more:<front>
  $menu_links['menu-footer-menu_learn-more:<front>'] = array(
    'menu_name' => 'menu-footer-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Learn more',
    'options' => array(
      'fragment' => 'block-menu-menu-footer-menu',
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-footer-menu_learn-more:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );
  // Exported menu link: menu-footer-menu_must-see:<front>
  $menu_links['menu-footer-menu_must-see:<front>'] = array(
    'menu_name' => 'menu-footer-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Must see',
    'options' => array(
      'fragment' => 'block-views-preview-block',
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-footer-menu_must-see:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
  );
  // Exported menu link: menu-footer-menu_random-facts:<front>
  $menu_links['menu-footer-menu_random-facts:<front>'] = array(
    'menu_name' => 'menu-footer-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Random facts',
    'options' => array(
      'fragment' => 'block-views-bigvideo-categories-block',
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-footer-menu_random-facts:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Boroughs');
  t('Home');
  t('Learn more');
  t('Must see');
  t('Random facts');

  return $menu_links;
}
