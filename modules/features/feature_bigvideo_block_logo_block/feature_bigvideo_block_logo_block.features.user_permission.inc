<?php
/**
 * @file
 * feature_bigvideo_block_logo_block.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function feature_bigvideo_block_logo_block_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer block classes'.
  $permissions['administer block classes'] = array(
    'name' => 'administer block classes',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'block_class',
  );

  // Exported permission: 'administer blocks'.
  $permissions['administer blocks'] = array(
    'name' => 'administer blocks',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'block',
  );

  return $permissions;
}
