<?php
/**
 * @file
 * feature_bigvideo_content.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function feature_bigvideo_content_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_image_default_styles().
 */
function feature_bigvideo_content_image_default_styles() {
  $styles = array();

  // Exported image style: 1170x900.
  $styles['1170x900'] = array(
    'label' => '1200x900',
    'effects' => array(
      3 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 1200,
          'height' => 900,
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}
