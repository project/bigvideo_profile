<?php

/**
 * @file
 * Select the current install profile and add additional checkbox to the form.
 */

/**
 * Allows the profile to alter the site configuration form.
 */
if (!function_exists("system_form_install_configure_form_alter")) {

  /**
   * Implements hook_form_FORM_ID_alter().
   */
  function system_form_install_configure_form_alter(&$form, $form_state) {
    $form['site_information']['site_name']['#default_value'] = 'bigvideo_profile';

    // Add new option at configure site form. If checkbox was selected, we
    // enable custom module, which sends usage statistics.
    $form['additional_settings'] = array(
      '#type' => 'fieldset',
      '#title' => st('Additional settings'),
      '#collapsible' => FALSE,
    );

    $form['additional_settings']['send_message'] = array(
      '#type' => 'checkbox',
      '#title' => st('Send info to developers team'),
      '#description' => st('Send reports to developers. We use anonymous data about your site (URL and site-name) to fix issues and ensure great user experience.'),
      '#default_value' => TRUE,
    );

    $form['#submit'][] = 'system_form_install_configure_form_custom_submit';
  }

  /**
   * Function system_form_install_configure_form_custom_submit().
   */
  function system_form_install_configure_form_custom_submit($form, &$form_state) {
    if ($form_state['values']['send_message']) {
      if (!module_exists('profile_stat_sender')) {
        module_enable(array('profile_stat_sender'), FALSE);
      }
    }
  }

}

/**
 * Select the current install profile by default.
 */
if (!function_exists("system_form_install_select_profile_form_alter")) {

  /**
   * Implements hook_form_alter().
   */
  function system_form_install_select_profile_form_alter(&$form, $form_state) {
    foreach ($form['profile'] as $key => $element) {
      $form['profile'][$key]['#value'] = 'bigvideo_profile';
    }
  }

}
