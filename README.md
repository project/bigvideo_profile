BigVideo Installation Profile
==================

BigVideo is an ideal multipurpose theme for designing portfolios, events, any advertising websites or blogs. The clean, trendy and fully responsive website template is built with Drupal and CSS3. It is a powerful tool to create a website in several clicks. The main feature of the theme is the integrated BigVideo module allowing to attach background video to the site. Background videos are trending because they give us an opportunity to add an appropriate atmosphere to the website.

It has two variations - black and white, it is easily customisable via admin panel. The profile includes a variety of blocks for showcasing whatever you need. 

Please feel free to add issues and ask about new features. Out profile is actively maintained and currently an alpha version is released. We are going to make it Bigger and Better.

The profile contains our BigVideo module. Check it if you need a video background for your website!

Created by ADCI solutions team
Supporting organizations: 
- ADCI Solutions

