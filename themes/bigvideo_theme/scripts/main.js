
(function ($) {
  Drupal.behaviors.bigvideo_theme = {
    attach: function (context, settings) {
      // Global variables
      window.last = null;

      function changeLabel (link) {
        $(window.last).removeClass("yes-visited");
        $(link).addClass("yes-visited");
        window.last = link;
      }

      // Local variables
      var home_link = null;
      var header_height = "";

      home_link = $("#block-system-main-menu .leaf.first a");
      if (!window.last) {
        home_link.addClass("yes-visited");
        home_link.addClass("internal");
        window.last = home_link.selector;
      }
      header_height = $("#header").outerHeight();

      // Adding class "internal" if the internal link.
      function linkClass(link) {
        var is_internal = $(link).attr('href').match(/\#[\w-]+/);
        if (is_internal) {
          $(link).addClass("internal");
        }
      }

      link_objs = $("#block-system-main-menu .menu a");
      var i = 0;
      for (i; i <= link_objs.length-1; i++) {
        linkClass(link_objs[i]);
      }

      $(document).on("scroll", function () {

        var cur_offset = $(document).scrollTop();
        var link_objs = {};

        var ref_offset = 0;
        var ref_obj = {};
        var i = 0; // index
        var tmp_retval;
        var b = true; // block is not found yet

        link_objs = $("#block-system-main-menu .menu a.internal");

        i = 0;
        tmp_retval = $(link_objs[i + 1]).attr('href').match(/\#[\w-]+/);
        if (tmp_retval && tmp_retval.length > 0) { // if link provide identifier
          ref_obj = $(tmp_retval[0]);
          if (ref_obj && ref_obj.length > 0) { // if link destination exist
            ref_offset = ref_obj.offset().top;
            if (cur_offset < ref_offset - parseInt(header_height) ) {
              changeLabel(link_objs[i]);
              b = false;
            }
          }
        }

        i = link_objs.length - 1;

        tmp_retval = $(link_objs[i]).attr('href').match(/\#[\w-]+/);
        if (b && tmp_retval && tmp_retval.length > 0) { // if link provide identifier
          ref_obj = $(tmp_retval[0]);
          if (ref_obj && ref_obj.length > 0) { // if link destination exist
            ref_offset = ref_obj.offset().top;

            if (ref_offset - cur_offset < $(window).height()) {
              changeLabel(link_objs[i]);
              b = false;
            }
          }
        }

        for (i = link_objs.length - 2;b && i > 0;i--) {
          tmp_retval = $(link_objs[i]).attr('href').match(/\#[\w-]+/);
          if (tmp_retval && tmp_retval.length > 0) { // if link provide identifier
            ref_obj = $(tmp_retval[0]);
            if (ref_obj && ref_obj.length > 0) { // if link destination exist
              ref_offset = ref_obj.offset().top;
              if (cur_offset > ref_offset - 2 * parseInt(header_height) ) {
                changeLabel(link_objs[i]);
                b = false;
              }
            }
          }
        }

      });

      $(".block-menu .leaf:not(.first) a, .continue-button-wrapper a, .footer-nav .leaf:not(.first) a").click(function(e){
        e.preventDefault();
        tmp_retval = $(this).attr('href').match(/\#[\w-]+/);
        if (tmp_retval && tmp_retval.length > 0) {
          ref_obj = $(tmp_retval[0]);
          if (ref_obj.length > 0) {
            $("html, body").stop().animate({ scrollTop: ref_obj.offset().top - parseInt(header_height) }, 700);
            return false;
          }
        }
        else {
          window.open(this.href,'_blank');
          return false;
        }
        return true;
      });

      $(".block-menu .first a, .footer-nav .first a").click(function(){

        $("html, body").animate({
            scrollTop: 1
        }, 500);
        return false;
      });

      $("#columns").css("padding-top", header_height);

    }
  };

  Drupal.behaviors.initialFullArea = {
    attach: function(context, settings) {
      var setArea = function() {
        var viewportHeight,
            headerHeight,
            mainContentHeight,
            adminMenuHeight,
            boxContinueHeight;

        if (typeof window.innerWidth != 'undefined') {
          viewportHeight = window.innerHeight;
        } else {
          viewportHeight = document.getElementsByTagName('body')[0].clientheight;
        }
        headerHeight = $('#header').outerHeight();
        mainContentHeight = $("#main-content").outerHeight();
        adminMenuHeight = $("#admin-menu-wrapper").outerHeight();

        boxContinueHeight = viewportHeight - (headerHeight + mainContentHeight + adminMenuHeight);
        $('#block-boxes-continue').css('height', boxContinueHeight);
      }

      $(window).load(setArea);

      var rtime;
      var timeout = false;
      var delta = 200;
      $(window).resize(function() {
          rtime = new Date();
          if (timeout === false) {
              timeout = true;
              setTimeout(resizeend, delta);
          }
      });

      function resizeend() {
          if (new Date() - rtime < delta) {
              setTimeout(resizeend, delta);
          } else {
              timeout = false;
              setArea();
          }
      }

      $(document).bind('cbox_complete', function(){
        setArea();
        $(document).trigger("scroll");
      });
    }
  };

  Drupal.behaviors.matchHeight = {
  attach: function(context, settings) {
      $('.block-boxes-simple').matchHeight();
    }
  };

  Drupal.behaviors.mobileMenu = {
  attach: function(context, settings) {
      $('.mobile-toggle').off('click.mobileMenu').on('click.mobileMenu', function(){
        $('#header nav').slideToggle();
      });
    }
  };
})(jQuery);
